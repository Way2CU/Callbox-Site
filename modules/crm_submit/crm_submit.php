<?php

/**
 * Submit data to webhook which then in turns submits it to CRM
 * for Callbox.
 *
 * Copyright © 2018 Way2CU. All Rights Reserved.
 * Author: Mladen Mijatov
 */
use Core\Module;
use Core\Events;


class crm_submit extends Module {
	private static $_instance;

	/**
	 * Constructor
	 */
	protected function __construct() {
		parent::__construct(__FILE__);

		Events::connect('contact_form', 'email-sent', 'handleEmailSent', $this);
	}

	/**
	 * Public function that creates a single instance
	 */
	public static function getInstance() {
		if (!isset(self::$_instance))
			self::$_instance = new self();

		return self::$_instance;
	}

	/**
	 * Transfers control to module functions
	 *
	 * @param array $params
	 * @param array $children
	 */
	public function transferControl($params = array(), $children = array()) {
	}

	/**
	 * Event triggered upon module initialization
	 */
	public function onInit() {
	}

	/**
	 * Event triggered upon module deinitialization
	 */
	public function onDisable() {
	}

	/**
	 * Handle `email-sent` event from contact form.
	 *
	 * @param string $mailer
	 * @param string $recipient
	 * @param string $subject
	 * @param array $data
	 */
	public function handleEmailSent($mailer, $recipient, $data) {
		$post_data = array();
		$post_data = array_merge($post_data, $data);

		// send data
		$url = 'https://scripts.way2cu.com/callboxform/leadstocrm.php';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HTTPGET, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);

		return true;
	}
}
